const bsConfig = {
  files: [`${__dirname}/public/css/*.css`],
  open: false,
  plugins: [{
    module: `bs-html-injector`,
    options: {
      files: [`public/*.html`]
    }
  }],
  server: {
    baseDir: `public`,
    serveStaticOptions: {
      extensions: [`html`]
    }
  }
};

require(`browser-sync`).init(bsConfig);
