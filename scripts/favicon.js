#!/usr/bin/env node
const
  favicons = require(`favicons`),

  faviconSrc = `assets/favicon/favicon.png`,
  faviconCfg = {
    appName: `wver`,
    appDescription: `wver home page`,
    developerName: `brwnrclse (Barry Harris)`,
    developerURL: `https://brwnrclse.xyz`,
    path: `/`,
    url: `wver.vaemoi.co`,
    display: `standalone`,
    orientation: `portrait`,
    version: `1.0`,
    logging: false,
    online: false,
    icons: {
      android: false,
      appleIcon: true,
      appleStartup: false,
      coast: false,
      favicons: true,
      firefox: false,
      opengraph: false,
      twitter: false,
      windows: false,
      yandex: false
    }
  },
  faviconCb = (err, res) => {
    if (err) {
      throw err;
    }

    res.images.map((image) => {
      if (image.name === `favicon.ico`) {
        require(`fs`).writeFileSync(`./public/${image.name}`,
          image.contents);
      }

      require(`fs`).writeFileSync(`./public/assets/favicon/${image.name}`,
        image.contents);
    });
  };

favicons(faviconSrc, faviconCfg, faviconCb);
